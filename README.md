# Mars 2052
**A lot of information/configuration/details have been removed or altered to remove any links to original assignment!**

This project represents a collaboration of multiple people, where we had to build a Martian startup centered around a single launch product which will lead to a better life on Mars.

Our idea was to create a product that lets you alternate your personality, by implementing a chip and controlling it via our site.
The customer has the possibility to buy, add and remove traits from profiles they bought and can configure to their likings.

- The server part is built in Java using the vert.x library, along with an H2 database to store all user data.
- The client is build in HTML, css and plain old JavaScript. (A huge thank you to Tuur Delacroix ([LinkedIn](https://www.linkedin.com/in/tuur-delacroix/), [GitHub](https://github.com/TuurDelacroix), [Website](http://tuurdelacroix.be/))) for doing most of the work in the client!)
- The API is based on the RESTful principles and built using OpenAPI.



Some of the technologies/principles used are:
- Graphs
- Maps
- Native drag 'n drop
- Fullscreen API
- Higher order functions on the server
- SASS (variables, nesting, ...)
- Push notifications
